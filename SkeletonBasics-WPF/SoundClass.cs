﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMPLib;

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    class SoundClass
    {
 // because it was complaining I you put SoundPlayer : WindowsMediaPlayerClass
        private WindowsMediaPlayer wmp;
        private bool locker;
        private String url;

        public SoundClass(String url)
            :base()
        {
            wmp = new WindowsMediaPlayer();
            wmp.uiMode = "invisible";
            // events
            wmp.PlayStateChange +=
                new _WMPOCXEvents_PlayStateChangeEventHandler(Player_PlayStateChange);
            wmp.MediaError +=
                new _WMPOCXEvents_MediaErrorEventHandler(Player_MediaError);
           wmp.Buffering += new _WMPOCXEvents_BufferingEventHandler(Player_Buffering);
           wmp.CurrentItemChange += new _WMPOCXEvents_CurrentItemChangeEventHandler(Player_CurrentItemChange);
           this.url = url;
           locker = true;
        }

        // handlers
        private void Player_PlayStateChange(int NewState)
        {
            Console.Write("play state change. " + NewState + "\n");
            if ((WMPLib.WMPPlayState)NewState == WMPLib.WMPPlayState.wmppsMediaEnded) // .wmppsMediaEnded  .wmppsStopped
            {
                locker = true;
                Console.Write("play stop " +  locker +"\n");
                
            }
            //if ((WMPLib.WMPPlayState)NewState == WMPLib.WMPPlayState.wmppsPlaying)  // .wmppsTransitioning   .wmppsPlaying
            //{
            //    //locker = false;
            //    Console.Write("play begin. " +  locker +"\n");
            //}
        }

        private void Player_MediaError(object pMediaObject)
        {
           // Console.Write("Cannot play media file.\n");
            //Environment.Exit(0);
        }

        void Player_CurrentItemChange(object pMediaObject)
        {
            //Console.Write("Change item.\n");
            //Do something when the current item changes.
        }

        void Player_Buffering(bool Start)
        {
            //Console.Write("player buffering.\n");
            //Do something when the player is buffering.
        }

        // to get the wmp variable
        public WindowsMediaPlayer getWMP()
        {
            return wmp;
        }

        //public void playSound()
        //{
        //   if (!locker)
        //    {
        //        wmp.controls.play();
        //    }

        //   //lock (wmp)
        //   //{
        //   //    wmp.controls.play();
        //   //}
        //}


        // play a sound with an url
        public void playSound()
        {

            //Console.Write("DSFSFSDFDFDSFA.\n");
            if (locker)
            {
                locker = false;
               // Console.Write("Hola.\n");
                wmp.URL = url;
                wmp.controls.play();
            }

            //lock (wmp)
            //{
            //    wmp.controls.play();
            //}
        }

    }
}
