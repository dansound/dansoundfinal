﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using Microsoft.Kinect;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    //using System.IO;
    using System.Linq;
    using System.Media;
    using System.Runtime.InteropServices;
    using System.Threading;
    // using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Threading;
    using WMPLib;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region sound player
        SoundClass player0 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\nord_ambient01.wav");
       // SoundClass player1 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\dong01.wav");
        SoundClass player2 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\acoustic guitar.wav");
        SoundClass player3 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar a2 2.wav");
        SoundClass player4 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar g3.wav");
        SoundClass player5 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar d3.wav");
        SoundClass player6 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar c3.wav");
        SoundClass player7 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar f3.wav");
        SoundClass player8 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar e3.wav");
        SoundClass player9 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar d3 2.wav");
        SoundClass player10 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar g3 2.wav");
        SoundClass player11 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar g3 2.wav");
       // SoundClass player12 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\hit01.wav");
        SoundClass player13 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar a3 2.wav");
        SoundClass player14 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\claps.mp3");
        SoundClass player15 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\acoustic guitar.wav");
     //   SoundClass player16 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\tom04.wav");
    //    SoundClass player17 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\dong02.wav");
        SoundClass player18 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar f3 2.wav");
        SoundClass player19 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\guitar a3 2.wav");
        SoundClass player20 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\explode01.wav");
        SoundClass player21 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\dong01.wav");
        SoundClass player22 = new SoundClass(@"G:\DanSound\dansoundproject\SkeletonBasics-WPF\Resources\dong02.wav");

        private void changeVolume(int value)
        {    
            //player1.getWMP().settings.volume = value;
            player2.getWMP().settings.volume = value;
            player3.getWMP().settings.volume = value;
            player4.getWMP().settings.volume = value;
            player5.getWMP().settings.volume = value;
            player6.getWMP().settings.volume = value;
            player7.getWMP().settings.volume = value;
            player8.getWMP().settings.volume = value;
            player9.getWMP().settings.volume = value;
            player10.getWMP().settings.volume = value;
            player11.getWMP().settings.volume = value;
            player13.getWMP().settings.volume = value;
            player14.getWMP().settings.volume = value;
            player15.getWMP().settings.volume = value;
            
            player18.getWMP().settings.volume = value;
            player19.getWMP().settings.volume = value;
            player20.getWMP().settings.volume = value;
            player21.getWMP().settings.volume = value;
            player22.getWMP().settings.volume = value;


        }

        private void changeRate(double value)
        {
           // player1.getWMP().settings.rate = value;
            player2.getWMP().settings.rate = value;
            player3.getWMP().settings.rate = value;
            player4.getWMP().settings.rate = value;
            player5.getWMP().settings.rate = value;
            player6.getWMP().settings.rate = value;
            player7.getWMP().settings.rate = value;
            player8.getWMP().settings.rate = value;
            player9.getWMP().settings.rate = value;
            player10.getWMP().settings.rate = value;

        }

        #endregion sound player



        #region dance function
        
            
        private void Dance(Skeleton[] skeletonData)
        {
            foreach (Skeleton skeleton in skeletonData)
            {

                float HandLeft_Z, HandRight_Z, ShoulderLeft_Z, ShoulderRight_Z, ShoulderCenter_X, ShoulderCenter_Y, HipCenter_X, HipCenter_Z, KneeLeft_X, AnkleLeft_X, ElbowLeft_X, AnkleRight_Y, ElbowRight_X, ElbowLeft_Y, ElbowRight_Y, HandLeft_Y, HandRight_Y, HandLeft_X, HandRight_X, WristRight_Y, WristLeft_Y, WristLeft_Z, WristLeft_X, WristRight_X, WristRight_Z, Head_X, Head_Y, Head_Z, KneeLeft_Y, AnkleLeft_Y;

                KneeLeft_X = skeleton.Joints[JointType.KneeLeft].Position.X;
                AnkleLeft_X = skeleton.Joints[JointType.AnkleLeft].Position.X;
                HandLeft_Y = skeleton.Joints[JointType.HandLeft].Position.Y;
                HandLeft_Z = skeleton.Joints[JointType.HandLeft].Position.Z;
                HandRight_Y = skeleton.Joints[JointType.HandRight].Position.Y;
                HandRight_Z = skeleton.Joints[JointType.HandRight].Position.Z;
                ShoulderCenter_X = skeleton.Joints[JointType.ShoulderCenter].Position.X;
                ShoulderCenter_Y = skeleton.Joints[JointType.ShoulderCenter].Position.Y;
                HipCenter_X = skeleton.Joints[JointType.HipCenter].Position.X;
                HipCenter_Z = skeleton.Joints[JointType.HipCenter].Position.Z;

                HandLeft_X = skeleton.Joints[JointType.HandLeft].Position.X;
                HandRight_X = skeleton.Joints[JointType.HandRight].Position.X;

                WristLeft_X = skeleton.Joints[JointType.WristLeft].Position.X;
                WristLeft_Y = skeleton.Joints[JointType.WristLeft].Position.Y;
                WristLeft_Z = skeleton.Joints[JointType.WristLeft].Position.Z;
                WristRight_X = skeleton.Joints[JointType.WristRight].Position.X;
                WristRight_Y = skeleton.Joints[JointType.WristRight].Position.Y;
                WristRight_Z = skeleton.Joints[JointType.WristRight].Position.Z;

                ElbowLeft_Y = skeleton.Joints[JointType.ElbowLeft].Position.Y;
                ElbowRight_Y = skeleton.Joints[JointType.ElbowRight].Position.Y;
                ElbowLeft_X = skeleton.Joints[JointType.ElbowLeft].Position.X;
                ElbowRight_X = skeleton.Joints[JointType.ElbowRight].Position.X;
                Head_X = skeleton.Joints[JointType.Head].Position.X;
                Head_Y = skeleton.Joints[JointType.Head].Position.Y;
                Head_Z = skeleton.Joints[JointType.Head].Position.Z;
                AnkleLeft_Y = skeleton.Joints[JointType.AnkleLeft].Position.Y;
                AnkleRight_Y = skeleton.Joints[JointType.AnkleRight].Position.Y;
                KneeLeft_Y = skeleton.Joints[JointType.KneeLeft].Position.Y;


                float KneeRight_Y, ShoulderRight_X, ShoulderRight_Y, ShoulderLeft_X, ShoulderLeft_Y, HipCenter_Y, FootLeft_Y, FootRight_Y;
                KneeRight_Y = skeleton.Joints[JointType.KneeRight].Position.Y;
                ShoulderRight_X = skeleton.Joints[JointType.ShoulderRight].Position.X;
                ShoulderRight_Y = skeleton.Joints[JointType.ShoulderRight].Position.Y;
                ShoulderRight_Z = skeleton.Joints[JointType.ShoulderRight].Position.Z;
                ShoulderLeft_X = skeleton.Joints[JointType.ShoulderLeft].Position.X;
                ShoulderLeft_Y = skeleton.Joints[JointType.ShoulderLeft].Position.Y;
                ShoulderLeft_Z = skeleton.Joints[JointType.ShoulderLeft].Position.Z;
                HipCenter_Y = skeleton.Joints[JointType.HipCenter].Position.Y;
                FootRight_Y = skeleton.Joints[JointType.FootRight].Position.Y;
                FootLeft_Y = skeleton.Joints[JointType.FootLeft].Position.Y;
                double distance_RightShH, distance_LeftShH, distance_hands, distance_LeftHF, distance_RightHF;

                //bones lengths

                double bonecolumn, bonetibia, boneradius;
                boneradius = Math.Sqrt((ElbowLeft_X - WristLeft_X) * (ElbowLeft_X - WristLeft_X) + (ElbowLeft_Y - WristLeft_Y) * (ElbowLeft_Y - WristLeft_Y));
                bonecolumn = Math.Sqrt((HipCenter_X - ShoulderCenter_X) * (HipCenter_X - ShoulderCenter_X) + (HipCenter_Y - ShoulderCenter_Y) * (HipCenter_Y - ShoulderCenter_Y));
                
                //Console.WriteLine("Bone: " + bone);
                bonetibia = Math.Sqrt((KneeLeft_X - AnkleLeft_X) * (KneeLeft_X - AnkleLeft_X) + (KneeLeft_Y - AnkleLeft_Y) * (KneeLeft_Y - AnkleLeft_Y));
               
                //scenario 1 = left hand position under knee left position
//Remove it
                if (WristLeft_Y < KneeLeft_Y)
                {
                    // ChangeVolume(area1, 100);
                    //player1.playSound();
                }
                //----------------------------------------------------------------------------------
                //scenario 2 = distance between hands large opened
                double distance;
                //distance = Math.Sqrt((HandLeft_X - HandRight_X) * (HandLeft_X - HandRight_X) + (HandLeft_Y - HandRight_Y) * (HandLeft_Y - HandRight_Y));
                distance = Math.Abs(HandLeft_X - HandRight_X);
                if (distance > 3 * bonecolumn && distance != 0 && bonecolumn != 0)
                {
                    //ChangeVolume(area2, 100);
                    player2.playSound();
                }

                //---------------------------------------------------------------------------------
                //scenario 3 =  hands arround head
                double distanceRH, distanceLH;
                distanceLH = Math.Sqrt((WristLeft_X - Head_X) * (WristLeft_X - Head_X) + (WristLeft_Y - Head_Y) * (WristLeft_Y - Head_Y));
                distanceRH = Math.Sqrt((WristRight_X - Head_X) * (WristRight_X - Head_X) + (WristRight_Y - Head_Y) * (WristRight_Y - Head_Y));

                if ((distanceLH < 0.15 && distanceRH < 0.15) && (distanceLH != 0 && distanceRH != 0))
                {
                    // ChangeVolume(area4, 100);
                   player3.playSound();
                }
                //---------------------------------------------------------------------------------
                //scenario with relative hands

                distance_LeftShH = Math.Asin(Math.Abs(ShoulderLeft_Y - HandLeft_Y) / Math.Abs(ShoulderLeft_X - HandLeft_X));
                distance_RightShH = Math.Asin(Math.Abs(ShoulderRight_Y - HandRight_Y) / Math.Abs(ShoulderRight_X - HandRight_X));
                double grade10 = 10 * Math.PI / 180;
                double grade30 = 30 * Math.PI / 180;
                double grade40 = 40 * Math.PI / 180;
                double grade60 = 60 * Math.PI / 180;
                double grade70 = 70 * Math.PI / 180;
                double grade90 = 90 * Math.PI / 180;

                if (grade10 < distance_LeftShH && distance_LeftShH < grade30 && (HandLeft_Y > ShoulderLeft_Y)) { player4.playSound(); }
                if (grade40 < distance_LeftShH && distance_LeftShH < grade60 && (HandLeft_Y > ShoulderLeft_Y)) { player5.playSound(); }
                if (grade70 < distance_LeftShH && distance_LeftShH < grade90 && (HandLeft_Y > ShoulderLeft_Y)) { player6.playSound(); }

                if (grade10 < distance_RightShH && distance_RightShH < grade30 && (HandRight_Y > ShoulderRight_Y)) { player7.playSound(); }
                if (grade40 < distance_RightShH && distance_RightShH < grade60 && (HandRight_Y > ShoulderRight_Y)) { player8.playSound(); }
                if (grade70 < distance_RightShH && distance_RightShH < grade90 && (HandRight_Y > ShoulderRight_Y)) { player9.playSound(); }

                 //---------------------------------------------------------------------------------
                //change the volume
                //if ((grade40 < distance_LeftShH && distance_LeftShH < grade60 && (HandLeft_Y < ShoulderLeft_Y)) && (HandRight_X > 1.2))
                //{
                //    Console.WriteLine("change volume before to:" + Math.Abs((int)HandRight_Y * 100));
                //    if (HandRight_Y * 100 >= 0 && HandRight_Y * 100 <= 100)
                //    {
                //        Console.WriteLine("change volume toooooooooo:" + HandRight_Y * 100);
                //        changeVolume(Math.Abs((int)HandRight_Y * 100));
                //    }
                //}

                //---------------------------------------------------------------------------------
                //scenario 10
                //distance_LeftShH = Math.Sqrt((HandLeft_X - ShoulderLeft_X) * (HandLeft_X - ShoulderLeft_X) + (HandLeft_Y - ShoulderLeft_Y) * (HandLeft_Y - ShoulderLeft_Y) + (HandLeft_Z - ShoulderLeft_Z) * (HandLeft_Z - ShoulderLeft_Z));
                //distance_RightShH = Math.Sqrt((HandRight_X - ShoulderRight_X) * (HandRight_X - ShoulderRight_X) + (HandRight_Y - ShoulderRight_Y) * (HandRight_Y - ShoulderRight_Y) + (HandRight_Z - ShoulderRight_Z) * (HandRight_Z - ShoulderRight_Z));
                // distance_RightShH = Math.Sqrt((ShoulderRight_X - HandRight_X) * (ShoulderRight_X - HandRight_X) + (ShoulderRight_Y - HandRight_Y) * (ShoulderRight_Y - HandRight_Y));
                //Console.WriteLine("distance left: " + distance_LeftShH + "dist right" + distance_RightShH);
                //if (distance_LeftShH < 0.05 && distance_LeftShH != 0)

                //the hands in front of the body, tom and jerry dance
                if (Math.Abs(HandRight_Z - ShoulderRight_Z) > 1.2 * bonecolumn)
                {
                   player10.playSound();
                }
                //if (distance_RightShH < 0.05 && distance_RightShH != 0)
                if (Math.Abs(HandLeft_Z - ShoulderLeft_Z) > 1.2 * bonecolumn) 
                {
                    player11.playSound();
                }

                //---------------------------------------------------------------------------------
                //scenario 11, like aplause
                distance_hands = Math.Sqrt((HandLeft_X - HandRight_X) * (HandLeft_X - HandRight_X) + (HandLeft_Y - HandRight_Y) * (HandLeft_Y - HandRight_Y));

                if (distance_hands < 0.1 && distance_hands != 0)
                {
                   player13.playSound();
                }

                //---------------------------------------------------------------------------------
                //scenario 12 aplause at the end of the dancing
                if (Math.Abs(Head_Z-HipCenter_Z) > 1.5*bonetibia)
                {
                   player14.playSound();
                }
                //---------------------------------------------------------------------------------
                //scenario 13
                if (Math.Abs(AnkleLeft_Y-AnkleRight_Y) > 0.2 * bonetibia)
                {
                   player15.playSound();
                }

          

                //---------------------------------------------------------------------------------
                //scenario 16
                if (Math.Abs(HipCenter_Y - HandLeft_Y) < 0.01 && Math.Abs(HipCenter_Y - HandLeft_Y) != 0)
                {
                   player18.playSound();
                }

                //---------------------------------------------------------------------------------
                //scenario 17

                if (Math.Abs(HipCenter_Y - HandRight_Y) < 0.01 && Math.Abs(HipCenter_Y - HandRight_Y) != 0)
                {
                    player19.playSound();
                }
                //----------------------------------------------------------------------------------
                //scenario 18
                if (Math.Abs(KneeLeft_Y - AnkleLeft_Y) < 0.9 * bonecolumn && bonecolumn != 0 && Math.Abs(KneeLeft_Y - AnkleLeft_Y) != 0)
                {
                    //player20.playSound();
                }

                // Add relative position between wrist and shoulders
                //scenario close
                if ( (Math.Abs(WristLeft_Y-WristRight_Y)<0.1) && ((WristLeft_X - WristRight_X) > 0.2) && (WristLeft_Z < Head_Z) && (WristRight_Z < Head_Z))
                {
                   // player21.playSound();
                    //Thread.Sleep(5000);
                    this.Close();
                    
                }
            }
        }

        #endregion dance function



        /// <summary>
        /// Width of output drawing
        /// </summary>
        private const float RenderWidth = 640.0f;

        /// <summary>
        /// Height of our output drawing
        /// </summary>
        private const float RenderHeight = 480.0f;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Thickness of body center ellipse
        /// </summary>
        private const double BodyCenterThickness = 10;

        /// <summary>
        /// Thickness of clip edge rectangles
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Brush used to draw skeleton center point
        /// </summary>
        private readonly Brush centerPointBrush = Brushes.Blue;

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Pink, 6);

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor sensor;

        /// <summary>
        /// Drawing group for skeleton rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Draws indicators to show which edges are clipping skeleton data
        /// </summary>
        /// <param name="skeleton">skeleton to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private static void RenderClippedEdges(Skeleton skeleton, DrawingContext drawingContext)
        {
            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, RenderHeight - ClipBoundsThickness, RenderWidth, ClipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, RenderWidth, ClipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, RenderHeight));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(RenderWidth - ClipBoundsThickness, 0, ClipBoundsThickness, RenderHeight));
            }
        }

        /// <summary>
        /// Execute startup tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSource = new DrawingImage(this.drawingGroup);

            // Display the drawing using our image control
            Image.Source = this.imageSource;

            // Look through all sensors and start the first connected one.
            // This requires that a Kinect is connected at the time of app startup.
            // To make your app robust against plug/unplug, 
            // it is recommended to use KinectSensorChooser provided in Microsoft.Kinect.Toolkit
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor = potentialSensor;
                    break;
                }
            }

            if (null != this.sensor)
            {
                // Turn on the skeleton stream to receive skeleton frames
                this.sensor.SkeletonStream.Enable();

                // Add an event handler to be called whenever there is new color frame data
                this.sensor.SkeletonFrameReady += this.SensorSkeletonFrameReady;

                // Start the sensor!
                try
                {
                    this.sensor.Start();
                }
                catch (IOException)
                {
                    this.sensor = null;
                }
            }

            if (null == this.sensor)
            {
                this.statusBarText.Text = Properties.Resources.NoKinectReady;
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (null != this.sensor)
            {
                this.sensor.Stop();
            }
        }

        /// <summary>
        /// Event handler for Kinect sensor's SkeletonFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            bool flagDance = false;
            Skeleton[] skeletons = new Skeleton[0];

            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletons);
                }
            }

            using (DrawingContext dc = this.drawingGroup.Open())
            {
                // Draw a transparent background to set the render size
                dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));
             
                if (skeletons.Length != 0)
                {
                    foreach (Skeleton skel in skeletons)
                    {
                        RenderClippedEdges(skel, dc);

                        if (skel.TrackingState == SkeletonTrackingState.Tracked)
                        {
                            this.DrawBonesAndJoints(skel, dc);

                            //Star function with a movement
                           
                        //   if (skel.Joints[JointType.HandRight].Position.Y > skel.Joints[JointType.Head].Position.Y)
                        //    {
                         //       flagDance = true;             
                          // }
                          // if (flagDance == true) { 
                               Dance(skeletons); 
                          // }
                        }
                        else if (skel.TrackingState == SkeletonTrackingState.PositionOnly)
                        {
                            dc.DrawEllipse(
                            this.centerPointBrush,
                            null,
                            this.SkeletonPointToScreen(skel.Position),
                            BodyCenterThickness,
                            BodyCenterThickness);
                        }
                    }
                }

                // prevent drawing outside of our render area
                this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));
            }
        }

        /// <summary>
        /// Draws a skeleton's bones and joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBonesAndJoints(Skeleton skeleton, DrawingContext drawingContext)
        {
            // Render Torso
            this.DrawBone(skeleton, drawingContext, JointType.Head, JointType.ShoulderCenter);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderRight);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.Spine);
            this.DrawBone(skeleton, drawingContext, JointType.Spine, JointType.HipCenter);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipLeft);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipRight);

            // Left Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderLeft, JointType.ElbowLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowLeft, JointType.WristLeft);
            this.DrawBone(skeleton, drawingContext, JointType.WristLeft, JointType.HandLeft);

            // Right Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderRight, JointType.ElbowRight);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowRight, JointType.WristRight);
            this.DrawBone(skeleton, drawingContext, JointType.WristRight, JointType.HandRight);

            // Left Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipLeft, JointType.KneeLeft);
            this.DrawBone(skeleton, drawingContext, JointType.KneeLeft, JointType.AnkleLeft);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleLeft, JointType.FootLeft);

            // Right Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipRight, JointType.KneeRight);
            this.DrawBone(skeleton, drawingContext, JointType.KneeRight, JointType.AnkleRight);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleRight, JointType.FootRight);

            // Render Joints
            foreach (Joint joint in skeleton.Joints)
            {
                Brush drawBrush = null;

                if (joint.TrackingState == JointTrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (joint.TrackingState == JointTrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, this.SkeletonPointToScreen(joint.Position), JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Maps a SkeletonPoint to lie within our render space and converts to Point
        /// </summary>
        /// <param name="skelpoint">point to map</param>
        /// <returns>mapped point</returns>
        private Point SkeletonPointToScreen(SkeletonPoint skelpoint)
        {
            // Convert point to depth space.  
            // We are not using depth directly, but we do want the points in our 640x480 output resolution.
            DepthImagePoint depthPoint = this.sensor.CoordinateMapper.MapSkeletonPointToDepthPoint(skelpoint, DepthImageFormat.Resolution640x480Fps30);
            return new Point(depthPoint.X, depthPoint.Y);
        }

        /// <summary>
        /// Draws a bone line between two joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw bones from</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="jointType0">joint to start drawing from</param>
        /// <param name="jointType1">joint to end drawing at</param>
        private void DrawBone(Skeleton skeleton, DrawingContext drawingContext, JointType jointType0, JointType jointType1)
        {
            Joint joint0 = skeleton.Joints[jointType0];
            Joint joint1 = skeleton.Joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == JointTrackingState.NotTracked ||
                joint1.TrackingState == JointTrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == JointTrackingState.Inferred &&
                joint1.TrackingState == JointTrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if (joint0.TrackingState == JointTrackingState.Tracked && joint1.TrackingState == JointTrackingState.Tracked)
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, this.SkeletonPointToScreen(joint0.Position), this.SkeletonPointToScreen(joint1.Position));
        }

        /// <summary>
        /// Handles the checking or unchecking of the seated mode combo box
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void CheckBoxSeatedModeChanged(object sender, RoutedEventArgs e)
        {
            if (null != this.sensor)
            {
                if (this.checkBoxSeatedMode.IsChecked.GetValueOrDefault())
                {
                    this.sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                }
                else
                {
                    this.sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
                }
            }
        }
    }
}